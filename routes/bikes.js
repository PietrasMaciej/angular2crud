var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var path = require('path');

var bike = mongoose.model('bikes', new mongoose.Schema({
    _id: String,
    brand: String,
    model: String,
    email: String
}));


router.post('/createBike', function(req, res, next) {
    bike.findOne({
      brand: req.body.brand
    }, function(error, result) {
       if(error) { console.log(error); }
       else {
         if(result === null) {
           new bike ({
             _id: mongoose.Types.ObjectId(),
             brand: req.body.brand,
             model: req.body.model,
             email: req.body.email
           }).save(function(error) {
              if(error) { console.log(error); }
              else {
                res.json({ success: "Dodano rekord" });
              }
           });
         } else {
           res.json({ error: "Ten rekod już istnieje."});
         }
       }
    });
});

router.post('/deleteBike', function(req, res, next) {
    bike.findOneAndRemove({
      brand: req.body.brand
    }, function (error) {
      if (error) {
      console.log(error);
    } else {
      res.json({ success: "Usunięto rekord" });
    }
    });
});

router.post('/editBike', function(req, res, next) {
    bike.findOneAndUpdate({
      _id: req.body.trololo
    }, {
      brand: req.body.newBrand,
      model: req.body.newModel,
      email: req.body.newEmail
    },function(error, result) {
      if(error) console.log(error);
      else {
        res.send("Zedytowano");
      }
    });
});

router.get('/getBikes',function(req,res,next){
  bike.find(function(error,result){
    if(error){
      console.log(error);
    }else{
      if(result === null){
        console.log("brak rekordów w bazie");
      }else{
        console.log(result);
        res.json(result);
      }
    }
  });
});

module.exports = router;
