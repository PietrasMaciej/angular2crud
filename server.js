var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');


var port = process.env.PORT || 3000;

mongoose.connect('mongodb://localhost/bike', function(error) {
  if(error) {
    console.log("Blad laczenia do bazy danych: " + error);
  } else {
    console.log("Polaczona z baza danych.");
  }
});

var app = express();
var path = __dirname + "/views/";
var routes = require('./routes/index');
var bikes = require('./routes/bikes');

app.use(express.static(__dirname));
app.use(bodyParser.json());
app.use('/', routes);
app.use('/bikes', bikes);

app.get('*', function(req, res, next) {
  res.sendFile(path + "index.html");
});
// app.get('/', function (req, res) {
//   res.sendFile(path + 'index.html');
// });

app.listen(port, function () {
    console.log('Serwer działa na porcie ' + port);
});
