import {bootstrap}    from 'angular2/platform/browser'
import {RouteComponent} from './route.component'
import {ROUTER_PROVIDERS, LocationStrategy, PathLocationStrategy} from 'angular2/router';
import {provide} from 'angular2/core';
import {HTTP_PROVIDERS} from 'angular2/http';

bootstrap(RouteComponent, [
  ROUTER_PROVIDERS, HTTP_PROVIDERS,
  provide(LocationStrategy, {useClass: PathLocationStrategy})]);
