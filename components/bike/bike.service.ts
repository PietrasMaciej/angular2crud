import {Injectable} from "angular2/core";
import {Bike} from "./bike";
import {Http,HTTP_PROVIDERS,Headers} from "angular2/http"

@Injectable()
export class BikeService{
  constructor(private http: Http) {}

  addBike(bike : Bike){
    var headers = new Headers();
    headers.append('Content-type', 'application/json');

    return this.http.post('/bikes/createBike', JSON.stringify(bike), {headers: headers})
    .map(response => response.json());
  };

  deleteBike(bike : Bike){
    var headers = new Headers();
    headers.append('Content-type', 'application/json');

    return this.http.post('/bikes/deleteBike', JSON.stringify(bike), {headers: headers})
    .map(response => response.json());
  };

  editBike(trololo: string, newBrand: string, newModel: string, newEmail: string){
    var headers = new Headers();
    headers.append('Content-type', 'application/json');

    var values = JSON.stringify({
        trololo: trololo,
        newBrand: newBrand,
        newModel: newModel,
        newEmail: newEmail
    });

    return this.http.post('/bikes/editBike', values, {headers: headers})
    .subscribe(
      response => {
        if(response) window.location.href = "/";
      }
    );
    //.map(response => response.json());
  };

  getBikes(){
    var headers = new Headers();
    headers.append('Content-type', 'application/json');
    return this.http.get('/bikes/getBikes', {headers: headers})
    .map(response => response.json());
  }
}
