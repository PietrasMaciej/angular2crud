export class Bike {
  constructor(brand: string, model: string, email: string) {
    this.brand = brand;
    this.model = model;
    this.email = email;
  }
  brand: string;
  model: string;
  email: string;
}
