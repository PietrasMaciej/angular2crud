import {Component} from 'angular2/core'
import {RouteConfig, Router, ROUTER_DIRECTIVES} from 'angular2/router'
import {Http, HTTP_PROVIDERS, Headers} from 'angular2/http'
import {FORM_DIRECTIVES, AbstractControl, FormBuilder, Validators} from 'angular2/common';
import {BikeService} from './bike.service';
import {Bike} from './bike';
import 'rxjs/add/operator/map';

@Component({
    selector: 'my-app',
    templateUrl: 'components/bike/addBike.html',
    styleUrls: ['components/bike/style.css'],
    directives: [ROUTER_DIRECTIVES, FORM_DIRECTIVES],
    providers: [BikeService]
})
export class BikeComponent {

  bikes : Object;

  myForm: ControlGroup;
  myVal: AbstractControl;
  myValTwo: AbstractControl;

  constructor(public BikeService: BikeService, fb: FormBuilder) {
  BikeService.getBikes()
  .subscribe(
  response =>
  {
    if (response) this.bikes = response;
  })

  this.myForm = fb.group ({
      "brand": ["", Validators.compose([
        Validators.required,
        this.myValValidator
      ])],
      "email": ["", Validators.compose([
        Validators.required,
        this.myValValidatorTwo
      ])]
  });
  this.myVal = this.myForm.controls["brand"];
  this.myValTwo = this.myForm.controls["email"];
}

serverAnswer: Object;


addBike (brand: string, model: string, email: string){
  this.BikeService.addBike(new Bike(brand, model, email))
  .subscribe(
    response =>
    {
      if (response.success) window.location.href = "/";
      if (response.error) this.serverAnswer = response;
    }
  )
}

myValValidator(control: Control) {
  if(control.value.length <3 || control.value.length >20) {
    return {"long": true}
  }
}

myValValidatorTwo(ctrl: Control) {
  if(!ctrl.value.match (/@./i)) {
    return {"reqMail": true}
  }
}


deleteBike (brand: string){
  this.BikeService.deleteBike(new Bike (brand))
  .subscribe(
    response =>
    {
      if (response.success) window.location.href = "/addBike";
      if (response.error) this.serverAnswer = response;
    }
  )
}

editBike (trololo: string, newBrand: string, newModel: string, newEmail: string){
  this.BikeService.editBike(trololo, newBrand, newModel, newEmail)
  // .subscribe(
  //   response =>
  //   {
  //     if (response.success) window.location.href = "/addBike";
  //     if (response.error) this.serverAnswer = response;
  //   }
  // )
}

}
