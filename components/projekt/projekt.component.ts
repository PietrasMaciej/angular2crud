import {Component} from 'angular2/core'
import {RouteConfig, Router, ROUTER_DIRECTIVES} from 'angular2/router'
import {Http, HTTP_PROVIDERS, Headers} from 'angular2/http'
import {BikeService} from '../bike/bike.service';
import {Bike} from 'component/bike/bike';

@Component({
    selector: 'my-app',
    templateUrl: 'components/projekt/menu.html',
    styleUrls: ['components/projekt/table.css'],
    directives: [ROUTER_DIRECTIVES],
    providers:   [BikeService]
})
export class ProjektComponent {
  bikes : Object;

  constructor(public BikeService: BikeService) {
  BikeService.getBikes()
  .subscribe(
  response =>
  {
    if (response) this.bikes = response;
  })
}
}
