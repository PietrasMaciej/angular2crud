import {Component} from 'angular2/core'
import {BikeComponent} from './bike/bike.component'
import {ProjektComponent} from './projekt/projekt.component'
import {RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router'

@RouteConfig([
  {
    path: '/',
    name: 'Main',
    component: ProjektComponent,
    useAsDefault: true
  },
  {
    path: '/addBike',
    name: 'AddBike',
    component: BikeComponent,
  }
])

@Component({
  selector: 'my-app',
  styleUrls: ['components/nav.css'],
  template: `
  <ul>
  <li><a class="active" [routerLink]="['Main']">Start</a></li>
  <li><a [routerLink]="['AddBike']">Formularz</a></li>
  </ul>
        <router-outlet></router-outlet>
  `,
  directives: [BikeComponent, ROUTER_DIRECTIVES]
})

export class RouteComponent { }
