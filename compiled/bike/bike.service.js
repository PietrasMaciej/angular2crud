System.register(["angular2/core", "angular2/http"], function(exports_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, http_1;
    var BikeService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            }],
        execute: function() {
            BikeService = (function () {
                function BikeService(http) {
                    this.http = http;
                }
                BikeService.prototype.addBike = function (bike) {
                    var headers = new http_1.Headers();
                    headers.append('Content-type', 'application/json');
                    return this.http.post('/bikes/createBike', JSON.stringify(bike), { headers: headers })
                        .map(function (response) { return response.json(); });
                };
                ;
                BikeService.prototype.deleteBike = function (bike) {
                    var headers = new http_1.Headers();
                    headers.append('Content-type', 'application/json');
                    return this.http.post('/bikes/deleteBike', JSON.stringify(bike), { headers: headers })
                        .map(function (response) { return response.json(); });
                };
                ;
                BikeService.prototype.editBike = function (trololo, newBrand, newModel, newEmail) {
                    var headers = new http_1.Headers();
                    headers.append('Content-type', 'application/json');
                    var values = JSON.stringify({
                        trololo: trololo,
                        newBrand: newBrand,
                        newModel: newModel,
                        newEmail: newEmail
                    });
                    return this.http.post('/bikes/editBike', values, { headers: headers })
                        .subscribe(function (response) {
                        if (response)
                            window.location.href = "/";
                    });
                    //.map(response => response.json());
                };
                ;
                BikeService.prototype.getBikes = function () {
                    var headers = new http_1.Headers();
                    headers.append('Content-type', 'application/json');
                    return this.http.get('/bikes/getBikes', { headers: headers })
                        .map(function (response) { return response.json(); });
                };
                BikeService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http])
                ], BikeService);
                return BikeService;
            })();
            exports_1("BikeService", BikeService);
        }
    }
});
