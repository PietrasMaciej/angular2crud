System.register([], function(exports_1) {
    "use strict";
    var Bike;
    return {
        setters:[],
        execute: function() {
            Bike = (function () {
                function Bike(brand, model, email) {
                    this.brand = brand;
                    this.model = model;
                    this.email = email;
                }
                return Bike;
            })();
            exports_1("Bike", Bike);
        }
    }
});
