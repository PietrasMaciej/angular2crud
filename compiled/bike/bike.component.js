System.register(['angular2/core', 'angular2/router', 'angular2/common', './bike.service', './bike', 'rxjs/add/operator/map'], function(exports_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, common_1, bike_service_1, bike_1;
    var BikeComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (bike_service_1_1) {
                bike_service_1 = bike_service_1_1;
            },
            function (bike_1_1) {
                bike_1 = bike_1_1;
            },
            function (_1) {}],
        execute: function() {
            BikeComponent = (function () {
                function BikeComponent(BikeService, fb) {
                    var _this = this;
                    this.BikeService = BikeService;
                    BikeService.getBikes()
                        .subscribe(function (response) {
                        if (response)
                            _this.bikes = response;
                    });
                    this.myForm = fb.group({
                        "brand": ["", common_1.Validators.compose([
                                common_1.Validators.required,
                                this.myValValidator
                            ])],
                        "email": ["", common_1.Validators.compose([
                                common_1.Validators.required,
                                this.myValValidatorTwo
                            ])]
                    });
                    this.myVal = this.myForm.controls["brand"];
                    this.myValTwo = this.myForm.controls["email"];
                }
                BikeComponent.prototype.addBike = function (brand, model, email) {
                    var _this = this;
                    this.BikeService.addBike(new bike_1.Bike(brand, model, email))
                        .subscribe(function (response) {
                        if (response.success)
                            window.location.href = "/";
                        if (response.error)
                            _this.serverAnswer = response;
                    });
                };
                BikeComponent.prototype.myValValidator = function (control) {
                    if (control.value.length < 3 || control.value.length > 20) {
                        return { "long": true };
                    }
                };
                BikeComponent.prototype.myValValidatorTwo = function (ctrl) {
                    if (!ctrl.value.match(/@./i)) {
                        return { "reqMail": true };
                    }
                };
                BikeComponent.prototype.deleteBike = function (brand) {
                    var _this = this;
                    this.BikeService.deleteBike(new bike_1.Bike(brand))
                        .subscribe(function (response) {
                        if (response.success)
                            window.location.href = "/addBike";
                        if (response.error)
                            _this.serverAnswer = response;
                    });
                };
                BikeComponent.prototype.editBike = function (trololo, newBrand, newModel, newEmail) {
                    this.BikeService.editBike(trololo, newBrand, newModel, newEmail);
                    // .subscribe(
                    //   response =>
                    //   {
                    //     if (response.success) window.location.href = "/addBike";
                    //     if (response.error) this.serverAnswer = response;
                    //   }
                    // )
                };
                BikeComponent = __decorate([
                    core_1.Component({
                        selector: 'my-app',
                        templateUrl: 'components/bike/addBike.html',
                        styleUrls: ['components/bike/style.css'],
                        directives: [router_1.ROUTER_DIRECTIVES, common_1.FORM_DIRECTIVES],
                        providers: [bike_service_1.BikeService]
                    }), 
                    __metadata('design:paramtypes', [bike_service_1.BikeService, common_1.FormBuilder])
                ], BikeComponent);
                return BikeComponent;
            })();
            exports_1("BikeComponent", BikeComponent);
        }
    }
});
