System.register(['angular2/core', './bike/bike.component', './projekt/projekt.component', 'angular2/router'], function(exports_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, bike_component_1, projekt_component_1, router_1;
    var RouteComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (bike_component_1_1) {
                bike_component_1 = bike_component_1_1;
            },
            function (projekt_component_1_1) {
                projekt_component_1 = projekt_component_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            }],
        execute: function() {
            RouteComponent = (function () {
                function RouteComponent() {
                }
                RouteComponent = __decorate([
                    router_1.RouteConfig([
                        {
                            path: '/',
                            name: 'Main',
                            component: projekt_component_1.ProjektComponent,
                            useAsDefault: true
                        },
                        {
                            path: '/addBike',
                            name: 'AddBike',
                            component: bike_component_1.BikeComponent,
                        }
                    ]),
                    core_1.Component({
                        selector: 'my-app',
                        styleUrls: ['components/nav.css'],
                        template: "\n  <ul>\n  <li><a class=\"active\" [routerLink]=\"['Main']\">Start</a></li>\n  <li><a [routerLink]=\"['AddBike']\">Formularz</a></li>\n  </ul>\n        <router-outlet></router-outlet>\n  ",
                        directives: [bike_component_1.BikeComponent, router_1.ROUTER_DIRECTIVES]
                    }), 
                    __metadata('design:paramtypes', [])
                ], RouteComponent);
                return RouteComponent;
            })();
            exports_1("RouteComponent", RouteComponent);
        }
    }
});
